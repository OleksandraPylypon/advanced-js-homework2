const ul = document.createElement("ul");

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

for (let i = 0; i < books.length; i++) {
  const book = books[i];
  if (book.author && book.name && book.price) {
    const li = document.createElement("li");
    const text = document.createTextNode(
      `${book.author}: ${book.name} - ${book.price}`
    );
    li.appendChild(text);
    ul.appendChild(li);
  }
}

try {
  for (let i = 0; i < books.length; i++) {
    const book = books[i];
    if (!book.name) {
      throw new SyntaxError("Неповні дані: відсутнє поле name");
    } else if (!book.price) {
      throw new SyntaxError("Неповні дані: відсутнє поле price");
    } else if (!book.author) {
      throw new SyntaxError("Неповні дані: відсутнє поле author");
    }
  }
} catch (err) {
  console.log("Error: " + err.message);
}

document.getElementById("root").appendChild(ul);
